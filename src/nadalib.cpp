#include "nadalib.hpp"
#include <boost/date_time.hpp>
#include <chrono>

std::vector<std::string> nada::tokenize(const std::string& text, const char token) {
    std::vector <std::string> teile;
    std::size_t anfang = 0;
    std::size_t ende = 0;
    while ((ende = text.find(token, anfang)) != std::string::npos) {
        teile.push_back(text.substr(anfang, ende - anfang));
        anfang = ende;
        anfang++;
    }
    teile.push_back(text.substr(anfang));
    return teile;
}

std::string nada::datetime() {
    char buffer[26];
    const time_t timer = time(nullptr);
    const struct tm* tm_info = localtime(&timer);
    strftime(buffer, 26, "%d.%m.%Y %H:%M", tm_info);
    return {buffer};
}

std::string nada::heute_tag() {
    const auto& heute = datetime();
    return {heute.substr(0, 2)};
}

std::string nada::heute_monat() {
    const auto& heute = datetime();
    return {heute.substr(3, 2)};
}

std::string nada::heute_jahr() {
    const auto& heute = datetime();
    return {heute.substr(6, 4)};
}

bool nada::ist_nummer(const std::string& s) {
    return !s.empty() && std::all_of(s.begin(), s.end(), ::isdigit);
}
