#pragma once

#include <vector>
#include <string>

/// Hilfsfunktionen
namespace nada {

    std::vector<std::string> tokenize(const std::string& text, const char token);

    /// Liefert das aktuelle Datum nach dem Schema "%d.%m.%Y %H:%M".
    std::string datetime();

    /// Heutiger Tag (dd).
    std::string heute_tag();

    /// Heutiger Tag (mm).
    std::string heute_monat();

    /// Heutiges Jahr (yy).
    std::string heute_jahr();

    /// Ist das eine Zahl? (Führende Nullen werden akzeptiert).
    bool ist_nummer(const std::string& s);

}
