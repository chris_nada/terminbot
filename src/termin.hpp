#pragma once

#include <cereal/types/string.hpp>
#include <cereal/types/list.hpp>

class Termin final {

public:

    typedef std::list<Termin> termin_container;

    Termin() = default;
    Termin(const std::string& tag,    const std::string& monat, const std::string& jahr,
           const std::string& stunde, const std::string& minute,
           const std::string& text,
           bool wiederholen);

    /// Öffentlicher Zugriff (const).
    static const termin_container& get_termine();

    /// Termin hinzufügen.
    static void add_termin(const Termin& termin);

    /// Termin löschen.
    static void loesche_termin(const uint64_t termin_id);

    /// Einzelnen Termin holen.
    static std::optional<Termin> get_termin(const uint64_t termin_id);

    /// Termine von heute.
    static termin_container get_termine_heute();

    /// Termine von morgen.
    static termin_container get_termine_morgen();

    /// Termine der nächsten 7 Tage.
    static termin_container get_termine_woche();

    /// Löscht alle Termine, die älter als gestern sind.
    static void aufraeumen();

    /// Einmalige ID, z.B. zum Löschen.
    uint64_t get_id() const;

    /// size() == 2
    const std::string& get_tag() const;

    /// Wochentag als deutsches Wort ausgeschrieben.
    const std::string& get_wochentag() const;

    /// size() == 2
    const std::string& get_monat() const;

    /// size() == 4
    const std::string& get_jahr() const;

    /// size() == 2
    const std::string& get_stunde() const;

    /// size() == 2
    const std::string& get_minute() const;

    /// Benutzerdefinierter Text (optional)
    const std::string& get_text() const;

    /// Wöchentlich wiederholen?
    bool is_wiederholen() const;

    /// Chronologischer Sorterator.
    bool operator<(const Termin& rhs) const;

    /// Serialisierung via Cereal.
    template<class Archiv> void serialize(Archiv& ar) {
        ar(id, tag, monat, jahr, stunde, minute, text, wiederholen);
    }

private:

    static termin_container& get_container();
    static termin_container laden();
    static void speichern();
    static uint64_t get_freie_id();

private:

    static constexpr const char* termin_datei = "termine.dat";

    uint64_t id;

    std::string tag;
    std::string monat;
    std::string jahr;

    std::string stunde;
    std::string minute;

    std::string text;

    bool wiederholen;

};
