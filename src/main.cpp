#include "terminbot.hpp"

int main() {
    while (true) {
        try {
            Terminbot terminbot;
            terminbot.start();
        } catch (...) { std::cerr << "Fataler Fehler\n"; }
    }
}
