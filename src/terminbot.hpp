#pragma once

#include <ostream>
#include <iostream>
#include <memory>
#include <atomic>
#include <tgbot/types/Message.h>

namespace TgBot { class Bot; }

class Terminbot final {

public:

    Terminbot();
    ~Terminbot();

    void start();
    void sende_termine_heute();
    void sende_termine_morgen();
    void sende_termine_woche();
    void sende_termine_alle();

private:

    /// Erwartetes Format von message->text [dd.mm] [hh:mm] [text] [optional: wöchentlich? (default=false)]
    void kommando_termin(std::shared_ptr<TgBot::Message> message) const;

private:

    TgBot::Bot* telegram;
    std::atomic_bool loop = false;
    int64_t chat_id;

    /// Loggers
    std::ostream& log     = std::cout;
    std::ostream& log_dbg = std::cout;
    std::ostream& log_err = std::cerr;

};
