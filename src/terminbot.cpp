#include "terminbot.hpp"
#include "inih/cpp/INIReader.h"
#include "termin.hpp"
#include "nadalib.hpp"
#include "aht10/aht10_driver.hpp"

#include <tgbot/tgbot.h>
#include <tgbot/net/BoostHttpOnlySslClient.h>

Terminbot::Terminbot() {
    INIReader config_txt("config.txt");
    if (config_txt.ParseError() < 0) {
        std::cerr << "Fehler beim Laden von config.txt\n";
    }
    const std::string api_key = config_txt.Get("konfiguration", "apikey", "");
    chat_id = std::stoll(config_txt.Get("konfiguration", "chatid", "config.ini Fehler"));
    std::cout << api_key << '\n';
    std::cout << chat_id << '\n';
    telegram =  new TgBot::Bot(api_key);

    // Termin anlegen
    auto termin_callback = [&](TgBot::Message::Ptr message) {
            if (chat_id != message->chat->id) return;
            kommando_termin(message);
    };
    telegram->getEvents().onCommand("termin", termin_callback);
    telegram->getEvents().onCommand("Termin", termin_callback);

    // Termin löschen
    telegram->getEvents().onCommand("lösch", [&](TgBot::Message::Ptr message) {
        if (chat_id != message->chat->id) return;
        try {
            const auto& tokens = nada::tokenize(message->text, ' ');
            const uint64_t id = std::stoull(tokens.at(1));
            const auto& size_alt = Termin::get_termine().size();
            const std::optional<Termin> termin = Termin::get_termin(id);
            const std::string text = termin ? termin->get_text() : " ohne Beschreibung.";
            Termin::loesche_termin(id);
            if (size_alt != Termin::get_termine().size()) telegram->getApi().sendMessage(message->chat->id, "Termin gelöscht. Termin war: " + text);
            else telegram->getApi().sendMessage(message->chat->id, "Ich konnte keinen Termin unter dieser Nummer finden.");
        } catch (const std::exception& e) {
            telegram->getApi().sendMessage(message->chat->id, "Tut mir leid, das habe ich leider nicht verstanden. Bitte schreib es so:\n"
                                                              "`\\lösch 1234`",
                                                              false, 0, std::make_shared<TgBot::GenericReply>(), "Markdown"
            );
        }
    });

    // Termine anzeigen:
    // /termine [heute] [morgen] [woche] [alle]
    telegram->getEvents().onCommand("zeig", [&](TgBot::Message::Ptr message) {
        if (chat_id != message->chat->id) return;
        if (message->text.find("heute") != std::string::npos)  { sende_termine_heute();  return; }
        if (message->text.find("morgen") != std::string::npos) { sende_termine_morgen(); return; }
        if (message->text.find("woche") != std::string::npos)  { sende_termine_woche();  return; }
        if (message->text.find("alle") != std::string::npos)   { sende_termine_alle();   return; }
        telegram->getApi().sendMessage(message->chat->id, "Ja, gut, aber bitte sag mir entweder\n"
                                                          "`\\zeig heute`\n"
                                                          "`\\zeig morgen`\n"
                                                          "`\\zeig woche` oder\n"
                                                          "`\\zeig alle`",
                                       false, 0, std::make_shared<TgBot::GenericReply>(), "Markdown"
        );
    });

    // Ping
    telegram->getEvents().onCommand("ping", [&](TgBot::Message::Ptr message) {
        log << "ping aus chat: " << message->chat->id << '\n';
        telegram->getApi().sendMessage(message->chat->id, "Pong.");
    });

    // Vers
    telegram->getEvents().onCommand("vers", [&](TgBot::Message::Ptr message) {
        if (chat_id != message->chat->id) return;
        std::string fehler;
        try {
            std::stringstream ss;
            TgBot::BoostHttpOnlySslClient http_client;
            const TgBot::Url url("https://dailyverses.net/de/zufalls-bibelvers");
            const std::string& response = http_client.makeRequest(url, {});

            const std::string start_token = "<span class=\"v1\">";
            const auto start = response.find(start_token) + start_token.size();
            const auto ende = response.find("</span><div class=\"vr\">");
            if (start != std::string::npos && ende != std::string::npos) {
                const std::string vers(response.begin() + start, response.begin() + ende);
                const std::string start_kapitel_token = "class=\"vc\">";
                const auto start_kapitel = response.find(start_kapitel_token, ende) + start_kapitel_token.size();
                const auto ende_kapitel = response.find("</a><a href=", ende);
                if (start_kapitel != std::string::npos && ende_kapitel != std::string::npos) {
                    const std::string kapitel(response.begin() + start_kapitel, response.begin() + ende_kapitel);
                    telegram->getApi().sendMessage(message->chat->id, vers + "\n\n" + kapitel);
                    return;
                } else throw std::runtime_error("Textstelle nicht gefunden");
            } else throw std::runtime_error("Text nicht gefunden");
        } catch (const std::exception& e) {
            telegram->getApi().sendMessage(message->chat->id,
                                           "Leider habe ich meine Bibel gerade verduddelt. (" + std::string(e.what()) + ")");
        }
    });

    // AHT-10 Temperatur
    telegram->getEvents().onCommand("temp", [&](TgBot::Message::Ptr message) {
        if (chat_id != message->chat->id) return;
        std::string ergebnis;
        try {
            AHT10 sensor;
            sensor.aht10_init();
            if (sensor.is_ready()) {
                std::stringstream ss;
                ss << "Hier sind es ";
                ss << std::fixed;
                ss.precision(1);
                ss << sensor.read_temperature();
                ss << "°C";
                ergebnis = ss.str();
            }
        } catch(const std::exception& e) { ergebnis = e.what(); }
        telegram->getApi().sendMessage(message->chat->id, ergebnis);
    });

    // Hilfe
    telegram->getEvents().onCommand("hilfe", [&](TgBot::Message::Ptr message) {
        if (chat_id != message->chat->id) return;
        telegram->getApi().sendMessage(message->chat->id, "Folgende Kommandos verstehe ich:\n"
                                                          "\n"
                                                          "Diese Hilfe hier anzeigen.\n"
                                                          "`/hilfe`\n"
                                                          "\n"
                                                          "Pong.\n"
                                                          "`/ping`\n"
                                                          "\n"
                                                          "Lokale Temperatur anzeigen:\n"
                                                          "`/temp`\n"
                                                          "\n"
                                                          "Zufällig ausgewählten Vers aus der Bibel anzeigen:\n"
                                                          "`/vers`\n"
                                                          "\n"
                                                          "Termin anlegen (Jahreszahl nach Monat ist optional; 'wöchentlich' ist optional):\n"
                                                          "`/termin [dd.mm] [hh:mm] [text] [wöchentlich]`\n"
                                                          "\n"
                                                          "Termin löschen:\n"
                                                          "`/lösch [Terminnummer]`\n"
                                                          "\n"
                                                          "Termine anzeigen:\n"
                                                          "`/zeig heute`\n"
                                                          "`/zeig morgen`\n"
                                                          "`/zeig woche`\n"
                                                          "`/zeig alle`",
                                       false, 0, std::make_shared<TgBot::GenericReply>(), "Markdown"
        );
    });
}

Terminbot::~Terminbot() {
    delete telegram;
}

void Terminbot::start() {
    log << "Terminbot::start() = " << nada::datetime() << '\n';
    static std::string heute = nada::heute_tag();
    loop = true;
    try {
        printf("Bot username: %s\n", telegram->getApi().getMe()->username.c_str());
        TgBot::TgLongPoll longPoll(*telegram);
        while (loop) {
            printf("Long poll...\n");
            longPoll.start();

            // Neuer Tag?
            if (heute != nada::heute_tag()) {
                heute = nada::heute_tag();
                Termin::aufraeumen();
                telegram->getApi().sendMessage(chat_id, "Guten Morgen zusammen!");
                sende_termine_heute();
            }
        }
    } catch (TgBot::TgException& e) {
        printf("error: %s\n", e.what());
    }
}

void Terminbot::sende_termine_heute() {
    const auto& termine_heute = Termin::get_termine_heute();
    if (termine_heute.empty()) telegram->getApi().sendMessage(chat_id, "Keine Termine heute.");
    else {
        telegram->getApi().sendMessage(chat_id, "Termine heute:");
        for (const auto& termin : termine_heute) {
            std::stringstream ss;
            ss << '*' << termin.get_stunde() << ':' << termin.get_minute() << "* " << termin.get_text();
            ss << " (" << termin.get_id() << ')';
            telegram->getApi().sendMessage(chat_id, ss.str(), false, 0, std::make_shared<TgBot::GenericReply>(), "Markdown");
        }
    }
}

void Terminbot::sende_termine_morgen() {
    const auto& termine_morgen = Termin::get_termine_morgen();
    if (termine_morgen.empty()) telegram->getApi().sendMessage(chat_id, "Keine Termine morgen.");
    else {
        telegram->getApi().sendMessage(chat_id, "Termine morgen:");
        for (const auto& termin : termine_morgen) {
            std::stringstream ss;
            ss << '*' << termin.get_stunde() << ':' << termin.get_minute() << "* " << termin.get_text();
            ss << " (" << termin.get_id() << ')';
            telegram->getApi().sendMessage(chat_id, ss.str(), false, 0, std::make_shared<TgBot::GenericReply>(), "Markdown");
        }
    }
}

void Terminbot::sende_termine_woche() {
    const auto& termine_woche = Termin::get_termine_woche();
    if (termine_woche.empty()) telegram->getApi().sendMessage(chat_id, "Keine Termine morgen.");
    else {
        telegram->getApi().sendMessage(chat_id, "Termine der nächsten 7 Tage:");
        for (const auto& termin : termine_woche) {
            std::stringstream ss;
            ss << termin.get_tag() << '.' << termin.get_monat() << ". *" << termin.get_wochentag() << ' ';
            ss << termin.get_stunde() << ':' << termin.get_minute() << "* " << termin.get_text();
            ss << " (" << termin.get_id() << ')';
            telegram->getApi().sendMessage(chat_id, ss.str(), false, 0, std::make_shared<TgBot::GenericReply>(), "Markdown");
        }
    }
}

void Terminbot::sende_termine_alle() {
    const auto& termine = Termin::get_termine();
    if (termine.empty()) telegram->getApi().sendMessage(chat_id, "Keine gespeicherten Termine.");
    else {
        telegram->getApi().sendMessage(chat_id, "Alle gespeicherten Termine:");
        for (const auto& termin : termine) {
            std::stringstream ss;
            ss << termin.get_tag() << '.' << termin.get_monat() << '.' << termin.get_jahr() << ' ';
            ss << termin.get_stunde() << ':' << termin.get_minute() << ' ' << termin.get_text();
            ss << " (" << termin.get_id() << ')';
            telegram->getApi().sendMessage(chat_id, ss.str(), false, 0, std::make_shared<TgBot::GenericReply>(), "Markdown");
        }
    }
}

void Terminbot::kommando_termin(std::shared_ptr<TgBot::Message> message) const {
    try {
        // Termin ausparsen
        const auto& s = message->text;
        const auto& tokens = nada::tokenize(s, ' ');
        for (unsigned i = 0; i < tokens.size(); ++i) log_dbg << "Token " << std::to_string(i) << "=" << tokens.at(i) << '\n';
        const std::string beschreibung(s.begin() + s.find(tokens.at(3)), s.end());
        const std::string tag   = tokens.at(1).substr(0, 2);
        const std::string monat = tokens.at(1).substr(3, 2);
        const std::string jahr  = tokens.at(1).size() == 10 ? tokens.at(1).substr(6, 4) : nada::heute_jahr();
        if (!nada::ist_nummer(tag) || !nada::ist_nummer(monat)) throw std::invalid_argument("Tag und/oder Monat wurde nicht verstanden.");

        std::string stunde = tokens.at(2).substr(0, 2);
        std::string minute = tokens.at(2).substr(3, 2);
        if (!nada::ist_nummer(stunde)) stunde = "";
        if (!nada::ist_nummer(minute)) minute = "";
        if (stunde.empty() || minute.empty()) throw std::invalid_argument("Es muss eine Uhrzeit im vorgegebenen Format angegeben werden.");

        const bool wiederholen = s.find("wöchentlich") != std::string::npos;

        // Termin speichern
        Termin termin(tag, monat, jahr, stunde, minute, beschreibung, wiederholen);
        Termin::add_termin(termin);
        std::stringstream ss;
        ss << '[' << termin.get_id() << "] ";
        ss << termin.get_tag() << '.' << termin.get_monat() << ' ';
        ss << termin.get_stunde() << ':' << termin.get_minute() << ' ' << termin.get_text();
        ss << (wiederholen ? " (Wöchentlich)" : " (Einmalig)");
        telegram->getApi().sendMessage(message->chat->id, "Ich habe folgenden Termin gespeichert:\n" + ss.str());
    } catch (const std::exception& e) {
        telegram->getApi().sendMessage(message->chat->id, "Tut mir leid, das habe ich leider nicht verstanden. Bitte schreib es so:\n"
                                                          "`\\termin [dd.mm] [hh:mm] [text] [wöchentlich]` also zum Beispiel:\n"
                                                          "`\\termin 07.08 17:00 Christians Geburtstag` oder:\n"
                                                          "`\\termin 15.05 07:30 Gelber Sack wöchentlich`\n"
                                                          "Falls missverständlich, kann man auch das Jahr zusätzlich angeben:\n"
                                                          "`\\termin 01.05.2020 19:30 Treffen Kulturbrauerei`\n"
                                                          "Hinweis: Datum und Uhrzeit müssen mit führenden Nullen beginnen.\n\n"
                                                          "Fehlerkode: " + std::string(e.what()),
                                       false, 0, std::make_shared<TgBot::GenericReply>(), "Markdown"
        );
    }
}
