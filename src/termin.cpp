#include "termin.hpp"
#include "nadalib.hpp"

#include <cereal/archives/xml.hpp>
#include <fstream>
#include <boost/date_time.hpp>

using serial_in  = cereal::XMLInputArchive;
using serial_out = cereal::XMLOutputArchive;

Termin::Termin(const std::string& tag, const std::string& monat, const std::string& jahr,
               const std::string& stunde, const std::string& minute,
               const std::string& text,
               bool wiederholen)
        :    id(get_freie_id()),
             tag(tag), monat(monat), jahr(jahr),
             stunde(stunde), minute(minute),
             text(text),
             wiederholen(wiederholen) {
}

const Termin::termin_container& Termin::get_termine() {
    return get_container();
}

Termin::termin_container& Termin::get_container() {
    static termin_container termine = laden();
    return termine;
}

Termin::termin_container Termin::laden() {
    static termin_container termine;
    try {
        if (std::ifstream in(termin_datei); in.good()) {
            serial_in s_in(in);
            s_in >> termine;
        }
    } catch(const std::exception& e) { std::cerr << "\tTermin::laden(): " << e.what() << '\n'; }
    std::cout << termine.size() << " Termine geladen.\n";
    return termine;
}

void Termin::speichern() {
    const auto& termine = get_termine();
    try {
        if (std::ofstream out(termin_datei); out.good()) {
            serial_out s_out(out);
            s_out << termine;
            std::cout << termine.size() << " Termine gespeichert.\n";
        }
    } catch (const std::exception& e) { std::cerr << "\tTermin::speichern(): " << e.what() << '\n'; }
}

uint64_t Termin::get_freie_id() {
    // ID ermitteln
    const auto& termine = get_termine();
    if (termine.empty()) return 0;
    else {
        auto max_id_termin_it = std::max_element(termine.begin(), termine.end(),[](const Termin& t1, const Termin& t2) {
            return t1.id < t2.id;
        });
        return max_id_termin_it->id + 1; // Höchste ID + 1
    }
}

void Termin::add_termin(const Termin& termin) {
    auto& container = get_container();
    container.push_back(termin);
    container.sort();
    speichern();
}

void Termin::loesche_termin(const uint64_t termin_id) {
    auto& termine = get_container();
    for (auto it = termine.begin(); it != termine.end(); it++) {
        if (it->id == termin_id) {
            termine.erase(it);
            break;
        }
    }
    speichern();
}

uint64_t Termin::get_id() const {
    return id;
}

const std::string& Termin::get_tag() const {
    return tag;
}

const std::string& Termin::get_monat() const {
    return monat;
}

const std::string& Termin::get_stunde() const {
    return stunde;
}

const std::string& Termin::get_minute() const {
    return minute;
}

const std::string& Termin::get_text() const {
    return text;
}

const std::string& Termin::get_wochentag() const {
    static std::vector<std::string> de_long_weekday_names {
                    "Sonntag", "Montag", "Dienstag", "Mittwoch", "Donnerstag", "Freitag", "Samstag"
    };
    const auto& datum = boost::gregorian::date_from_iso_string(get_jahr() + get_monat() + get_tag());
    return de_long_weekday_names.at(datum.day_of_week());
}

bool Termin::is_wiederholen() const {
    return wiederholen;
}

bool Termin::operator<(const Termin& rhs) const {
    if (jahr < rhs.jahr) return true;
    if (rhs.jahr < jahr) return false;
    if (monat < rhs.monat) return true;
    if (rhs.monat < monat) return false;
    if (tag < rhs.tag) return true;
    if (rhs.tag < tag) return false;
    if (stunde < rhs.stunde) return true;
    if (rhs.stunde < stunde) return false;
    if (minute < rhs.minute) return true;
    if (rhs.minute < minute) return false;
    if (id < rhs.id) return true;
    if (rhs.id < id) return false;
    if (wiederholen < rhs.wiederholen) return true;
    if (rhs.wiederholen < wiederholen) return false;
    return text < rhs.text;
}

Termin::termin_container Termin::get_termine_heute() {
    termin_container termine;
    const auto& heute = nada::heute_tag();
    const auto& monat = nada::heute_monat();
    const auto& jahr  = nada::heute_jahr();
    for (const auto& termin : get_termine()) if (termin.tag == heute && termin.monat == monat && termin.jahr == jahr) termine.push_back(termin);
    return termine;
}

Termin::termin_container Termin::get_termine_morgen() {
    termin_container termine;
    const auto& morgen = boost::gregorian::date_from_iso_string(nada::heute_jahr() + nada::heute_monat() + nada::heute_tag()) + boost::gregorian::days(1);
    for (const auto& termin : get_termine()) {
        const auto termin_datum = boost::gregorian::date_from_iso_string(termin.jahr + termin.monat + termin.tag);
        if (termin_datum.day() == morgen.day() && termin_datum.month() == morgen.month() && termin_datum.year() == morgen.year()) {
            termine.push_back(termin);
        }
    }
    return termine;
}

Termin::termin_container Termin::get_termine_woche() {
    termin_container termine;
    const auto& jetzt = boost::gregorian::date_from_iso_string(nada::heute_jahr() + nada::heute_monat() + nada::heute_tag());
    for (const auto& termin : get_termine()) {
        const auto termin_datum = boost::gregorian::date_from_iso_string(termin.jahr + termin.monat + termin.tag);
        if (termin_datum - jetzt < boost::gregorian::days(8)) termine.push_back(termin);
    }
    return termine;
}

std::optional<Termin> Termin::get_termin(const uint64_t termin_id) {
    for (const auto& termin : get_termine()) if (termin.id == termin_id) return termin;
    return std::nullopt;
}

const std::string& Termin::get_jahr() const {
    return jahr;
}

void Termin::aufraeumen() {
    std::vector<uint64_t> zu_loeschende_termine; // IDs der zu löschenden Termine
    const auto& gestern = boost::gregorian::date_from_iso_string(nada::heute_jahr() + nada::heute_monat() + nada::heute_tag()) - boost::gregorian::days(1);
    for (const auto& termin : get_termine()) {
        const auto termin_datum = boost::gregorian::date_from_iso_string(termin.jahr + termin.monat + termin.tag);

        // Löschen?
        if (termin_datum < gestern) {

            // Termin wöchentlich: Kopieren vor Löschen
            if (termin.is_wiederholen()) {
                try {
                    const auto neues_datum = termin_datum + boost::gregorian::weeks(1);
                    const auto neues_datum_string = boost::gregorian::to_iso_string(neues_datum);
                    const auto neuer_tag = neues_datum_string.substr(6, 2);
                    const auto neuer_monat = neues_datum_string.substr(4, 2);
                    const auto neues_jahr = neues_datum_string.substr(0, 4);
                    Termin neuer_termin(neuer_tag, neuer_monat, neues_jahr, termin.stunde, termin.minute, termin.text,
                                        termin.wiederholen);
                    add_termin(neuer_termin);
                } catch (const std::exception& e) {
                    std::cerr << "Fehler beim Wiederholen des Termins. Datum konnte nicht geparst werden.\n";
                }
            }
            zu_loeschende_termine.push_back(termin.get_id());
        }
    }
    for (auto id : zu_loeschende_termine) loesche_termin(id);
}
