#pragma once

#include <unistd.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <linux/i2c-dev.h>
#include <cstdint>

#define AHT10_ADDR 0x38
#define AHT10_INIT_CMD     0xE1 
#define AHT10_SOFT_RESET   0xBA
#define AHT10_TRIG_MEAS    0xAC
#define AHT10_DAT1_CMD     0x33
#define AHT10_DAT2_CMD     0x00
#define HUMIDITY    0
#define TEMPERATURE 1

class AHT10 final {

public:

    const float ERROR_VALUE_A =  -256.0f;
    const float ERROR_VALUE_B =  -512.0f;
    const float ERROR_VALUE_C = -1024.0f;
    const float ERROR_VALUE_D = -2048.0f;

    void aht10_init();

    bool is_ready();

    float read_temperature();

    float read_humidity();

    static void delay(long ms);

private:

    float read_sensor(uint8_t temp_or_hum);

    bool ready = false;

    //Commands to trigger measurement
    const uint8_t trig_cmd[3] = {AHT10_TRIG_MEAS, AHT10_DAT1_CMD, AHT10_DAT2_CMD};

    //Calibration commands
    const uint8_t calib_cmd[3] = {0xE1, 0x08, 0x00};

    int fd;
    int length;

};
