#include "aht10_driver.hpp"

#include <cstdio>
#include <cmath>
#include <cstdint>
#include <ctime>
#include <thread>
#include <iostream>

void AHT10::aht10_init() {
    ready = false;
	//Open I2C Bus
	char* filename = (char*)"/dev/i2c-1";
	if ((fd = open(filename, O_RDWR)) < 0) throw std::runtime_error("Kein Zugriff auf I2C via Linux-Schnittstelle");

	if (ioctl(fd, I2C_SLAVE, AHT10_ADDR) < 0) throw std::runtime_error("Kein I2C-SLAVE an AHT10_ADDR vorhanden");

   	length = 3;
	if (write(fd, calib_cmd, length) != length) throw std::runtime_error("Schreiben auf I2C fehlgeschlagen\n");
    ready = true;
}

float AHT10::read_humidity() {
    const auto value = read_sensor(HUMIDITY);
    if (value == ERROR_VALUE_A || value == ERROR_VALUE_B || value == ERROR_VALUE_C || value == ERROR_VALUE_D) ready = false;
    return value;
}

float AHT10::read_temperature() {
    const auto value = read_sensor(TEMPERATURE);
    if (value == ERROR_VALUE_A || value == ERROR_VALUE_B || value == ERROR_VALUE_C || value == ERROR_VALUE_D) ready = false;
    return value;
}

float AHT10::read_sensor(uint8_t temp_or_hum) {
        uint8_t rx_buf[6];
        
        //Send trigger command
		length = 3;			
		if (write(fd, trig_cmd, length) != length) return ERROR_VALUE_A;
        
        // Sensor arbeitet
        delay(100);
        
        // Daten lesen
		length = 6;			
		if (read(fd, rx_buf, length) != length) return ERROR_VALUE_B;
		
        // Beschäftigt? Neuer Versuch
        while(rx_buf[0] & (1 << 7)) if (read(fd, rx_buf, length) != length) return ERROR_VALUE_C;
        
        const uint32_t humidity_reading = ((uint32_t)rx_buf[1] << 12) | ((uint16_t)rx_buf[2] << 4) | (rx_buf[3] >> 4) ;
        const uint32_t temp_reading = (((uint32_t)rx_buf[3] & 0x0F) << 16) | ((uint16_t)rx_buf[4] << 8) | (rx_buf[5]) ;
        
        const float humidity    = (((float)humidity_reading) / (pow(2,20))) * 100;
        const float temperature = ((((float)temp_reading)    / (pow(2,20))) * 200) - 50;
        
        if (temp_or_hum == HUMIDITY) return (humidity * 10.f) / 10.f;
        if (temp_or_hum == TEMPERATURE) return (temperature * 10.f) / 10.f;
        return ERROR_VALUE_D;
}

void AHT10::delay(long ms) {
    typedef std::chrono::duration<long> long_ms;
    std::this_thread::sleep_for(std::chrono::duration_cast<long_ms>(std::chrono::milliseconds(ms)));
}

bool AHT10::is_ready() {
    // TODO init retry
    return ready;
}
