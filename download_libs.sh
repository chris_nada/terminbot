#!/bin/sh

mkdir libs

if [ ! -d "libs/tgbot-cpp" ]; then
  wget --output-document=libs/tgbot.zip https://github.com/reo7sp/tgbot-cpp/archive/refs/tags/v1.2.1.zip
  unzip libs/tgbot.zip -d libs
  rm libs/tgbot.zip
  mv libs/tgbot-cpp-1.2.1 libs/tgbot-cpp
fi

if [ ! -d "src/inih" ]; then
  wget --output-document=src/inih.zip https://github.com/benhoyt/inih/archive/refs/tags/r53.zip
  unzip src/inih.zip -d src
  rm src/inih.zip
  mv src/inih-r53 src/inih
  rm -r src/inih/tests
  rm -r src/inih/examples
fi
